package net.pl3x.iconstranslator;

import javax.swing.ImageIcon;

import net.pl3x.iconstranslator.gui.IconLabel;
import net.pl3x.iconstranslator.gui.IconsTranslatorGUI;

public class Main {
	public static void main(String[] args) {
		IconLabel.preLoadImages();
		new IconsTranslatorGUI().setVisible(true);
	}

	protected static ImageIcon createImageIcon(String path, String description) {
		java.net.URL imgURL = Main.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}
}
