package net.pl3x.iconstranslator.gui;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import net.pl3x.iconstranslator.Icon;

public class IconLabel extends JLabel implements MouseListener {
	private static final long serialVersionUID = 1L;
	private static List<Image> pages = new ArrayList<Image>();
	private static final int perPage = 256;
	private static final int perRow = 16;
	private static final int perCol = 16;
	private static final int size = 16;

	private Icon icon;

	public IconLabel(Icon icon) {
		super();

		this.icon = icon;

		int id = icon.getId();
		String code = icon.getCode();

		int page = id == 0 ? 0 : (int) Math.ceil(id / perPage);
		int row = (int) Math.ceil((id % perPage) / perRow);
		int col = id % perCol;

		setOpaque(true);
		setHorizontalAlignment(JLabel.CENTER);
		setBorder(new EmptyBorder(5, 0, 5, 0));
		setBackground(UIManager.getColor("Button.background"));
		setIcon(new ImageIcon(createImage(new FilteredImageSource(pages.get(page).getSource(), new CropImageFilter(col * size, row * size, size, size)))));
		setToolTipText("{" + code + "}");

		addMouseListener(this);
	}

	public static void preLoadImages() {
		for (int i = 0; i < 10; i++) {
			ImageIcon icon = new ImageIcon(IconLabel.class.getResource("/images/unicode_page_9" + i + ".png"));
			pages.add(i, icon.getImage().getScaledInstance(256, 256, java.awt.Image.SCALE_SMOOTH));
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		setBackground(Color.CYAN);
		LeftPanel.inputBox.setText(LeftPanel.inputBox.getText() + "{" + icon.getCode() + "}");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		RightPanel.mouseDown = true;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		RightPanel.mouseDown = false;
		setBackground(RightPanel.bgColor);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (RightPanel.mouseDown) {
			return;
		}
		setBackground(Color.BLUE);
		RightPanel.statusBar.setText("{" + icon.getCode() + "}");
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (RightPanel.mouseDown) {
			return;
		}
		setBackground(RightPanel.bgColor);
		RightPanel.statusBar.setText("");
	}
}
