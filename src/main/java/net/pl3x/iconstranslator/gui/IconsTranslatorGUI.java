package net.pl3x.iconstranslator.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.UIManager;

import net.pl3x.iconstranslator.LogoLoader;

public class IconsTranslatorGUI extends JFrame {
	private static final long serialVersionUID = 1L;

	public IconsTranslatorGUI() {
		setBackground(new Color(238, 238, 238));
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			Logger.getLogger(getName()).log(Level.SEVERE, null, e);
		}

		setType(Type.UTILITY);
		setAlwaysOnTop(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImages(LogoLoader.getIcons());
		setTitle("Pl3xIcons Translator");
		setMinimumSize(new Dimension(800, 500));
		setBackground(UIManager.getColor("Button.background"));

		getContentPane().setLayout(null);
		getContentPane().add(new LeftPanel());
		getContentPane().add(new RightPanel());

		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
}
