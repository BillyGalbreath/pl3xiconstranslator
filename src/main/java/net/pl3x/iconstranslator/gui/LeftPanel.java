package net.pl3x.iconstranslator.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import net.pl3x.iconstranslator.Icon;

public class LeftPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public static JEditorPane inputBox;
	public static JEditorPane outputBox;

	public LeftPanel() {
		setBorder(null);
		setBounds(0, 0, 263, 470);
		setLayout(null);
		setBackground(UIManager.getColor("Button.background"));

		inputBox = new JEditorPane();
		inputBox.setBounds(12, 12, 239, 200);
		inputBox.setBorder(BorderFactory.createEtchedBorder(1));

		outputBox = new JEditorPane();
		outputBox.setBounds(12, 258, 239, 200);
		outputBox.setBorder(BorderFactory.createEtchedBorder(1));

		JButton btnTranslate = new JButton("Translate");
		btnTranslate.setBounds(12, 224, 117, 25);
		btnTranslate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				outputBox.setText(Icon.translate(inputBox.getText()));
			}
		});

		JButton btnDecode = new JButton("Decode");
		btnDecode.setBounds(134, 224, 117, 25);
		btnDecode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				outputBox.setText(Icon.untranslate(inputBox.getText()));
			}
		});

		add(inputBox);
		add(outputBox);
		add(btnTranslate);
		add(btnDecode);
	}
}
