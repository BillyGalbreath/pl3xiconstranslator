package net.pl3x.iconstranslator.gui;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import net.pl3x.iconstranslator.Icon;

public class RightPanel extends JLayeredPane {
	private static final long serialVersionUID = 1L;
	public static Color bgColor;
	public static StatusBar statusBar;
	public static boolean mouseDown = false;

	public RightPanel() {
		setBorder(null);
		setBounds(263, 0, 529, 470);
		setLayout(null);
		setOpaque(true);
		setBackground(UIManager.getColor("Button.background"));

		JScrollPane iconScrollPane = new JScrollPane();
		iconScrollPane.setViewportBorder(BorderFactory.createEtchedBorder(1));
		iconScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		iconScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		iconScrollPane.setBounds(0, 4, 528, 444);
		iconScrollPane.setBackground(UIManager.getColor("Button.background"));

		final JPanel scrollPort = new JPanel();
		scrollPort.setBorder(new EmptyBorder(10, 10, 10, 0));
		scrollPort.setBounds(0, 0, 508, 470);
		scrollPort.setLayout(new GridLayout(0, 16, 0, 5));
		scrollPort.setBackground(UIManager.getColor("Button.background"));
		bgColor = scrollPort.getBackground();
		iconScrollPane.setViewportView(scrollPort);

		statusBar = new StatusBar();

		add(statusBar, 2, 0);
		add(iconScrollPane, 1, 0);

		new Thread() {
			public void run() {
				Icon[] icons = Icon.values();
				int count = 1;
				int total = icons.length;
				for (Icon icon : icons) {
					scrollPort.add(new IconLabel(icon));
					statusBar.setText("Loading Icons... (" + count + "/" + total + ")");
					count++;
					try {
						Thread.sleep(0);
					} catch (InterruptedException ignore) {
					}
				}
				statusBar.setText("");
			}
		}.start();
	}
}
