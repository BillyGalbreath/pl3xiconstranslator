package net.pl3x.iconstranslator.gui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.SystemColor;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class StatusBar extends JPanel {
	private static final long serialVersionUID = 1L;

	private BufferedImage buffer;
	private boolean isFading = false;
	private long start;
	private float alpha = 1.0f;

	JLabel statusText;

	public StatusBar() {
		setOpaque(true);
		setBackground(SystemColor.controlHighlight);
		setBorder(new LineBorder(new Color(128, 128, 128)));
		setBounds(0, 450, 528, 17);
		setLayout(null);

		statusText = new JLabel("");
		statusText.setFont(new Font("Dialog", Font.PLAIN, 12));
		statusText.setVerticalAlignment(SwingConstants.TOP);
		statusText.setHorizontalAlignment(SwingConstants.LEFT);
		statusText.setBackground(Color.LIGHT_GRAY);
		statusText.setBounds(10, 1, 250, 15);

		add(statusText);
	}

	public void setText(String text) {
		statusText.setText(text);
	}

	@Override
	public void paint(Graphics g) {
		if (isFading) {// During fading, we prevent child components from being painted
			g.clearRect(0, 0, getWidth(), getHeight());
			((Graphics2D) g).setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
			g.drawImage(buffer, 0, 0, this);// We only draw an image of them with an alpha
		} else {
			super.paint(g);
		}
	}

	public void fade(final int time) {
		final StatusBar bar = this;
		new Thread() {
			public void run() {
				start = System.currentTimeMillis();
				buffer = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
				bar.print(buffer.getGraphics());// Draw the current components on the buffer
				isFading = true;
				while (isFading) {
					long elapsed = System.currentTimeMillis() - start;
					if (elapsed > time) {
						bar.setVisible(false);
						start = 0;
						isFading = false;
						buffer = null;
						repaint();
					} else {
						try {
							Thread.sleep(10);
						} catch (InterruptedException ignore) {
						}
						alpha = (float) Math.abs(Math.sin(1F - (float) elapsed / time));
						repaint();
					}
				}
			}
		}.start();
	}
}
